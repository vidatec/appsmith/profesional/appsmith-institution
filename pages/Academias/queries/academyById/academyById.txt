query academy($id: String!) {
    academy(id: $id) {
      id
      name
      courses {
        id
        title
      }
    }
  }