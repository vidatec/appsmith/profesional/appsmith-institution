export default {
	async assignAccountToInstitution() {
		try {
			for (const id of MultiSelectRole.selectedOptionValues) {
				await storeValue('currentInstitutionId', id);
				await add_user.run();
			}
			await accountByDni.data.data.accountByDni.institutions;
			showAlert('Asociación creada correctamente')
		} catch (error) {
			showAlert(error.message, 'error')
		}
	}
}