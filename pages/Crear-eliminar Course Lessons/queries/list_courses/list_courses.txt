SELECT 
t1.id AS course_id, 
t1.academy_id, 
t2.id, 
t1.title AS course_title, 
t2.name AS academy_name,
t1.deleted_at AS academy_deleted_at,
t2.deleted_at AS course_deleted_at
FROM public.courses t1 
JOIN public.academies t2 
ON uuid(t1.academy_id) = t2.id 
WHERE t1.deleted_at IS NULL AND t2.deleted_at IS NULL
ORDER BY title ASC;
