export default {
  sendUpdateQuestion() {
    // Crear FormData
    const formData = new FormData();
    formData.append('id', up_question_question_id.text);
    formData.append('id', up_question_question_id.text);
    formData.append('question_text', up_question_question_text.text);
    formData.append('question_type', up_question_question_type.selectedOptionValue);
    formData.append('is_mandatory', up_question_is_mandatory.isChecked);
    
		if(up_question_point.text !== '') {
		formData.append('point', up_question_point.text);	
		}
		if(up_question_order.text !== '') {
		formData.append('point', up_question_point.text);	
		}
		if(up_question_description.text !== '') {
		formData.append('point', up_question_point.text);	
		}
    // Agregar archivo si existe
    if (FilePicker_question_update.files.length > 0) {
      formData.append('file', FilePicker_question_update.files[0]);
    }
    
    // Llamar a la API usando fetch
    return fetch('http://api-academies.profesional.svc.cluster.local:4040/v1/exam-questions', {
      method: 'PUT',
      body: formData, // Enviar el FormData directamente
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(HTTP error! Status: ${response.status});
        }
        return response.json();
      })
      .then(data => {
        showAlert('Se actualizo la pregunta.', 'success');
        return data;
      })
      .catch(error => {
        showAlert(No se pudo actualizar: ${error.message}, 'error');
        throw error;
      });
  },
};