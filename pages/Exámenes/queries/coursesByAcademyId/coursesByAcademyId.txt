query courses($academyId: String!) {
    courses(academyId: $academyId) {
      id
      title
      course_config {
        id
        course_type
        navigation_mode
      }
    }
  }