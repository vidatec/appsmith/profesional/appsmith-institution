{
	"id": {{up_question_question_id.text}},
	"question_text": {{up_question_question_text.text}},
	"question_type": {{up_question_question_type.selectedOptionValue}},
	"is_mandatory": {{up_question_is_mandatory.isChecked}},
	"description": {{up_question_description.text !== '' ? up_question_description.text : undefined}},
	"order": {{up_question_order.text !== null ? up_question_order.text : undefined}},
	"point": {{up_question_point.text}}
}
