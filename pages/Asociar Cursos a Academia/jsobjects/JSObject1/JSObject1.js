export default {
	
  
		getUniqueCourses: () => {
    const uniqueCoursesMap = {};
    List_Courses.data.data.forEach(course => {
      uniqueCoursesMap[course.resource_id] = {
        label: `${course.title} --- ID del recurso (RM): ${course.resource_id}`, 
        value: course.id
      };
    });

     const uniqueCourses = Object.values(uniqueCoursesMap);
    return uniqueCourses.sort((a, b) => a.label.localeCompare(b.label));
  }

  
	


}