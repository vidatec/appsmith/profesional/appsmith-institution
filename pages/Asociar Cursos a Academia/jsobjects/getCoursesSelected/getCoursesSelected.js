export default {

	getCourses: () => {
		const courses = List_Courses.data.data?.map(course => course) ?? []
		const ids = Courses_Input?.selectedOptionValues
		
		const filteredCourses = courses?.filter(course => ids?.includes(course?.id))
	
		return filteredCourses.map(course => {
			const { id, academy_id, updated_at, created_at, deleted_at, ...rest } = course
			
			return {...rest, rubric_path: ''}
		})
	}
}
