SELECT a.id, a.name, a.uri, ca.course_id, c.title
FROM assets AS a
JOIN courses_assets AS ca ON a.id = ca.asset_id
JOIN courses AS c ON ca.course_id = c.id;