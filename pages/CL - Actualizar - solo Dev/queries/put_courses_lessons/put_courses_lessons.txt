{
	"id": {{lesson_id.inputText}},
	"title": {{lesson_title.inputText}},
	"course_id": {{lesson_course_id.text}},
	"asset_id": {{courses_lessons.triggeredRow.asset_id}},
	"rise_lesson_id": {{courses_lessons.triggeredRow.rise_lesson_id}},
	"lesson_order": {{Number(lesson_order.inputText)}},
	"lesson_type": {{lesson_type.selectedOptionValue}}
}