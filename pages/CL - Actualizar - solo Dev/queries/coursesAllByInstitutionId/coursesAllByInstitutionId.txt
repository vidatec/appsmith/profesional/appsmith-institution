query coursesAllByInstitutionId($institutionId: String!){
  coursesAllByInstitutionId(institutionId: $institutionId) {
    id
    title
    academy {
      name
			id
    }
		lessons {
        id
        title
        lesson_type
      }
  }
}


