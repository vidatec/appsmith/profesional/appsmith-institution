export default {
	getProjects: async () => {
		if (Tutor_Filter.selectedOptionValue !== null && Tutor_Filter.selectedOptionValue !== undefined && Tutor_Filter.selectedOptionValue !== '') {
			await get_projects_with_tutor.run()
			return get_projects_with_tutor.data
		} else {
			await get_projects.run()
			return get_projects.data
		}
	},
	unassignTutor: () => {
		console.log(JSON.stringify(Projects.selectedRows.map(r => {
			return {
				...r,
				tutor_id: null,
				status: "IN_REVIEW"
			}}
		)))
	}
}